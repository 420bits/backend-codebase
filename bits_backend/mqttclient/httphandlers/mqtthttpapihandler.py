# https://stackoverflow.com/questions/19475955/using-django-models-in-external-python-script
from django.core import serializers
from rest_framework.utils import json
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from app_example.models import Question
from app_example.serializers import SnippetSerializer
from mqttclient.httphandlers.basehandler import BaseHandler
from mqttclient.mqttconnectionclient import MQTTConnectionClient
from mqttclient.models.mqttmessage import MQTTMessage


class MQTTHTTPAPIHandler(BaseHandler):

    mqtt_client = None

    def initialize(self, mqtt_client):
        self.mqtt_client = mqtt_client

    def get(self):
        snippets = Question.objects.all()
        serializer = SnippetSerializer(snippets, many=True)
        json_response = JsonResponse(serializer.data, safe=False)
        self.mqtt_client.publish("/test/ac", "Hello")
        self.set_status(json_response.status_code)
        self.write(json_response.content)

    def post(self):
        data = json.loads(self.request.body)
        serializer = SnippetSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            json_response = JsonResponse(serializer.data, status=201)
            self.set_status(json_response.status_code)
            self.write(json_response.content)
        else:
            json_response = JsonResponse(serializer.errors, status=400)
            self.set_status(json_response.status_code)
            self.write(json_response.content)

#!/bin/bash

FILE_SOURCE="${BASH_SOURCE[0]}"
while [ -h "$FILE_SOURCE" ]; do # resolve $FILE_SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$FILE_SOURCE" )" && pwd )"
  FILE_SOURCE="$(readlink "$FILE_SOURCE")"
  [[ $FILE_SOURCE != /* ]] && FILE_SOURCE="$DIR/$FILE_SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$FILE_SOURCE" )" && pwd )"

LOGS_PATH="$DIR/logs"
SCRIPTS_PATH=$DIR
RESTART="0"
STOP="0"
LIST_SERVICE="0"

declare -a scripts=("bits_backend/tornado_api.py")

for i in "$@"
do
case $i in
    -r|--restart)
    RESTART="1"
    ;;

    -s|--stop)
    STOP="1"
    ;;

    -d|--delete-logs)
    rm $LOGS_PATH/*
    echo "Logs deleted in path: $LOGS_PATH"
    ;;

    -l|--list)
    LIST_SERVICE="1"
    ;;

    # -l=*|--lib=*)
    # DIR="${i#*=}"
    # ;;

    --default)
    DEFAULT=YES
    ;;

    *)
    ;;
esac
done

if [ "$RESTART" == "1" ] || [ "$STOP" == "1" ]; then
    sudo $SCRIPTS_PATH/emqttd/bin/emqttd stop
fi

if [ "$STOP" == "0" ]; then
    sudo $SCRIPTS_PATH/emqttd/bin/emqttd start
fi


for script in "${scripts[@]}"
do
   	if [ "$RESTART" == "1" ] || [ "$STOP" == "1" ]; then
   		PID=`ps -eaf | grep $script | grep -v grep | awk '{print $2}'`
		if [[ "" !=  "$PID" ]]; then
		  echo "Killing $PID - Script: $script"
		  sudo kill -9 $PID
		fi
   	fi

    if [ "$LIST_SERVICE" == "1" ]; then
        ps -aux | grep -v grep | grep "$script"

    elif [ "$STOP" == "0" ]; then
        if (( $(ps -aux | grep -v grep | grep "$script" | wc -l) > 0 ))
        then
        echo "$script is already running"
        else
        echo "Will start $script"
        sudo mkdir -p -m 777 "$LOGS_PATH"
        source venv/bin/activate
        python3.7 $SCRIPTS_PATH/$script >> "$LOGS_PATH/start.log" 2>&1 &
        echo "Did start $script"
        fi
    fi
done

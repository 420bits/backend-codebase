from mqttclient.models.mqttmessage import MQTTMessage
from app_example.models import Question


class DefaultMQTTHandler(object):
    def handle(self, mqtt_message):
        questions = Question.objects.all()
        print("    Did Dispatch MQTT message: " + mqtt_message.topic + " - " + str(mqtt_message.payload))
        print("      - Models:" + str(questions))

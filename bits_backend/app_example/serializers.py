from rest_framework import serializers
from .models import Question


class SnippetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        fields = ('question_text', 'id', 'pub_date')

from mqttclient.models.mqttmessage import MQTTMessage
from mqttclient.mqtthandlers.defaultmqtthandler import DefaultMQTTHandler


class MQTTMessageDispatcher(object):

    handler = None

    def __init__(self, mqtt_handler):
        self.handler = mqtt_handler

    def handling_topics(self):
        topics = ["test/ab"]
        return topics

    def dispatch(self, topic, message, mqtt_client):
        mqtt_message = MQTTMessage(topic=topic, payload=message)
        print("Will dispatch...")
        self.handler.handle(mqtt_message=mqtt_message)

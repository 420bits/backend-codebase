import json


class MQTTMessage:
    topic = None
    payload = None
    def __init__(self, topic, payload):
        self.topic = topic
        self.payload = payload

    def payload_json(self):
        try:
            return json.loads(self.payload)
        except:
            return {}

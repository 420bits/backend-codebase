from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('snippets/', views.SnippetList.as_view()),
    path('snippets/<int:pk>/', views.SnippetDetail.as_view())
]

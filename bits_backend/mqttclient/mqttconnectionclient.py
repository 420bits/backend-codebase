import paho.mqtt.client as mqtt
import json
from mqttclient.mqttmessagedispatcher import MQTTMessageDispatcher


class MQTTConnectionClient(object):
    type = ''

    client = None
    dispatcher = None

    def __init__(self, client_id, mqtt_dispatcher):
        self.dispatcher = mqtt_dispatcher
        self.client = mqtt.Client(client_id=client_id, clean_session=True, userdata=None)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.onDisconnect

    def connect(self, host, port):
        self.client.connect_async(host, port=port, keepalive=60, bind_address="")

    def subscribe(self, topic):
        self.client.subscribe(topic)

    def unsubscribe(self, topic):
        self.client.unsubscribe(topic)

    def publish(self, topic, message, retain=False):
        if isinstance(message, dict):
            message = json.dumps(message)
        self.client.publish(topic=topic, payload=str(message), retain=retain, qos=0)

    def publish_response(self, from_socket_message, response, retain=False):
        topic = from_socket_message.topic + "/response"
        if isinstance(response, dict):
            response = json.dumps(response)

        self.publish(topic=topic, message=str(response), retain=retain)

    def onDisconnect(self, client, userdata, rc):
        print("disonnected " + str(rc))

    def on_connect(self, client, userdata, flags, rc):
        print("Connected to MQTT " + str(rc))
        if self.dispatcher is not None:
            topics = self.dispatcher.handling_topics()
            for topic in topics:
                try:
                    client.subscribe(topic)
                except:
                    pass


    def on_message(self, client, userdata, msg):
        print("Received MQTT message: " + msg.topic+ " - " + str(msg.payload))
        if self.dispatcher is not None:
            self.dispatcher.dispatch(topic=msg.topic, message=msg.payload, mqtt_client=self)

    def loop_forever(self):
        self.client.loop_start()

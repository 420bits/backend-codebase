import tornado
from tornado import httpserver, ioloop, web, wsgi
from tornado.options import options, define, parse_command_line
import sys
import os
import django.core.handlers.wsgi

os.environ['DJANGO_SETTINGS_MODULE'] = 'bits_backend.settings'
django.setup()

from mqttclient.httphandlers.mqtthttpapihandler import MQTTHTTPAPIHandler
from mqttclient.mqttconnectionclient import MQTTConnectionClient
from mqttclient.mqttmessagedispatcher import MQTTMessageDispatcher
from mqttclient.mqtthandlers.defaultmqtthandler import DefaultMQTTHandler

mqtt_handler = DefaultMQTTHandler()
mqtt_dispatcher = MQTTMessageDispatcher(mqtt_handler=mqtt_handler)
mqtt_client = MQTTConnectionClient(client_id="9890909", mqtt_dispatcher=mqtt_dispatcher)
mqtt_client.connect(host="192.168.0.108", port=1883)

def main():
    parse_command_line()
    tornado_app = tornado.web.Application(
            [('/mqtt', MQTTHTTPAPIHandler, dict(mqtt_client=mqtt_client)),
     ])
    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(8888)
    mqtt_client.loop_forever()
    tornado.ioloop.IOLoop.instance().start()

if __name__ == '__main__':
    main()
